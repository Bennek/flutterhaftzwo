import 'package:flutter/material.dart';
import 'package:multi_tab/pages/fullpage_4bubbles.dart';
import 'package:multi_tab/pages/fullpage_barchart.dart';
import 'package:multi_tab/pages/fullpage_chart.dart';
import 'package:multi_tab/pages/fullpage_image.dart';
import './pages/main_page.dart';
import './pages/project_page.dart';
import './pages/details_page.dart';
import './pages/bubble_page.dart';
import 'pages/bubble_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.blueGrey,
        // textTheme: TextTheme(
        //   bodyText2: TextStyle(color: Colors.white),
        // )
      ),
      home: MyHomePage(title: 'Robert\'s Flutter Demo App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = new TabController(length: 9, vsync: this);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          //backgroundColor: Colors.deepOrange,
          // bottom: TabBar(
          //   controller: controller,
          //   tabs: <Tab>[
          //     Tab(icon: Icon(Icons.accessibility_new)),
          //     Tab(icon: Icon(Icons.apps)),
          //     Tab(icon: Icon(Icons.details)),
          //   ]
          // ),
        ),
        drawer: Drawer(
          elevation: 16.0,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                // DrawerHeader(
                //   decoration: BoxDecoration(
                //   gradient: LinearGradient(colors: <Color>[
                //     Colors.deepOrange, Colors.deepOrangeAccent
                //   ]),
                //   ),
                //   child: Text("Header"),
                // ),
                SizedBox(height: 100,),
                MenuItem4Me(
                  icon: Icons.person,
                  title: "Bubbles",
                  subtitle: "Shows nice bubbles",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullBubblesPage();
                    }));
                  },
                ),
                MenuItem4Me(
                  icon: Icons.access_alarm,
                  title: "Alerts",
                  subtitle: "Shows nice alerts",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullLinePages();
                    }));
                  },
                ),
                MenuItem4Me(
                  icon: Icons.pages,
                  title: "Dashboards",
                  subtitle: "Shows nice dashboards",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullBubblesPage();
                    }));
                  },
                ),
                MenuItem4Me(
                  icon: Icons.add_to_queue,
                  title: "Backlog Management",
                  subtitle: "Nice Backlog Management Sites",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullBubblesPage();
                    }));
                  },
                ),
                MenuItem4Me(
                  icon: Icons.accessibility_new,
                  title: "XYZ Management",
                  subtitle: "Nice Backlog Management Sites",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullImagePage();
                    }));
                  },
                ),
                MenuItem4Me(
                  icon: Icons.account_balance,
                  title: "Billing",
                  subtitle: "Nice Backlog Management Sites",
                  onTap: () {
                    debugPrint("First Item  $context");
                    Navigator.of(context).pop();
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return FullBarChartPage();
                    }));
                  },
                ),
              ],
            ),
          ),
        ),
        bottomNavigationBar: Material(
          color: Colors.blueGrey,
          child: TabBar(controller: controller, isScrollable: true, tabs: <Tab>[
            Tab(
              icon: Icon(Icons.accessibility_new),
              text: "Main",
            ),
            Tab(
              icon: Icon(Icons.apps),
              text: "Apps",
            ),
            Tab(
              icon: Icon(Icons.details),
              text: "Details",
            ),
            Tab(
              icon: Icon(Icons.accessibility_new),
              text: "Bubble",
            ),
            Tab(
              icon: Icon(Icons.apps),
              text: "Apps",
            ),
            Tab(
              icon: Icon(Icons.details),
              text: "Details",
            ),
            Tab(
              icon: Icon(Icons.accessibility_new),
              text: "Main",
            ),
            Tab(
              icon: Icon(Icons.apps),
              text: "Apps",
            ),
            Tab(
              icon: Icon(Icons.details),
              text: "Details",
            ),
          ]),
        ),
        body: TabBarView(controller: controller, children: <Widget>[
          MainPage(),
          ProjectPage(),
          DetailsPage(),
          BubblePage(),
          ProjectPage(),
          DetailsPage(),
          MainPage(),
          ProjectPage(),
          DetailsPage(),
        ]));
  }
}

class MenuItem4Me extends StatelessWidget {
  const MenuItem4Me({this.icon, this.title, this.subtitle, this.onTap});

  final IconData icon;
  final String title;
  final String subtitle;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        splashColor: Colors.deepOrangeAccent,
        hoverColor: Colors.deepOrange,
        highlightColor: Colors.blueGrey,
        child: ListTile(
          onTap: onTap,
          title: Text(
            title,
            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          subtitle: Text(subtitle,
              style: TextStyle(fontSize: 12.0, color: Colors.grey.shade900)),
          leading: Icon(
            icon,
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }
}
