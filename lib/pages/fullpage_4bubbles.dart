import 'package:flutter/material.dart';

import 'bubble_page.dart';

class FullBubblesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Bubbles 4 You"),
        elevation: 16.0,
      ),
      body: BubblePage(),
    );
  }
}