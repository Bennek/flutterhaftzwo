import 'package:flutter/material.dart';

import 'project_page.dart';


class FullLinePages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Lines 4 You"),
        elevation: 16.0,
      ),
      body: ProjectPage(),
    );
  }
}