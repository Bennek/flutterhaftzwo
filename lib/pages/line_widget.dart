/// Example of a simple line chart.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleLineChart(this.seriesList, {this.animate});

  /// Creates a [LineChart] with sample data and no transition.
  factory SimpleLineChart.withSampleData() {
    return new SimpleLineChart(
      createSampleData(),
      // Disable animations for image tests.
      animate: false,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.LineChart(seriesList, animate: animate);
  }


  // @override
  // Widget build(BuildContext context) {
  //   var axis = charts.NumericAxisSpec(
  //       renderSpec: charts.GridlineRendererSpec(
  //           labelStyle: charts.TextStyleSpec(
  //               fontSize: 10, color: charts.MaterialPalette.white),
  //           lineStyle: charts.LineStyleSpec(
  //               thickness: 0,
  //               color: charts.MaterialPalette.gray.shadeDefault)));

  //   return new charts.LineChart(seriesList,
  //       primaryMeasureAxis: axis,
  //       domainAxis: axis,
  //       animate: animate,
  //       customSeriesRenderers: [
  //         new charts.LineRendererConfig(
  //             // ID used to link series to this renderer.
  //             customRendererId: 'customArea',
  //             includeArea: true,
  //             stacked: true),
  //       ]);
  // }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> createSampleData() {
    final data = [
      new LinearSales(0, 5),
      new LinearSales(1, 25),
      new LinearSales(2, 100),
      new LinearSales(3, 75),
      new LinearSales(4, -10),
      new LinearSales(5, -40),
      new LinearSales(6, -2),
      new LinearSales(7, 34),
      new LinearSales(8, 42),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}