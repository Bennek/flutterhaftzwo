import 'package:flutter/material.dart';
import 'chart_widget.dart';

class DetailsPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: 
      Padding(
        padding: const EdgeInsets.all(18.0),
        //child: SimpleBarChart(SimpleBarChart.createSampleData()),
        child: SimpleBarChart.withSampleData(),
      ),)
    );
  }
}