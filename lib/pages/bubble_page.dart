import 'package:flutter/material.dart';
import 'bubble_chart.dart';

class BubblePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: 
      Padding(
        padding: const EdgeInsets.all(18.0),
        child: BucketingAxisScatterPlotChart(BucketingAxisScatterPlotChart.createSampleData()),
      ),)
    );
  }
}