import 'package:flutter/material.dart';
import 'line_widget.dart';

class ProjectPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: SimpleLineChart(SimpleLineChart.createSampleData()),
      ),)
    );
  }
}