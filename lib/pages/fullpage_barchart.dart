import 'package:flutter/material.dart';

import 'details_page.dart';


class FullBarChartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Bars 4 You"),
        elevation: 16.0,
      ),
      body: DetailsPage(),
    );
  }
}