import 'package:flutter/material.dart';


class FullImagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Lines 4 You"),
        elevation: 16.0,
      ),
      body: Center(
        child: Image.network("https://linuxwell.files.wordpress.com/2011/08/bridge1.png",
          fit:BoxFit.fill
        ),
      ),
    );
  }
}