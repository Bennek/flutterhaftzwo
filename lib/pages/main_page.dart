import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class MainPage extends StatelessWidget {
  final logger = Logger();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: MenuGrid(),
        ),
      ),
    );
  }

  void _onPressed() {
    logger.w("button was pressed");
    String msg = "hello melmac";
    print("hello ... $msg \n");
  }
}

class MenuGrid extends StatelessWidget {
  int getColumnCount(double width) {
    if (width < 600) {
      return 1;
    } else if (width < 1000) {
      return 3;
    } else if (width < 1600){
      return 4;
    }else{
      return 6;
    }
  }

  double getFontSize(double width) {
    if (width < 600) {
      return 0.7;
    } else if (width < 1000) {
      return 0.8;
    } else if (width < 1600){
      return 0.9;
    }else{
      return 1.0;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GridView.count(
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this produces 2 rows.
      crossAxisCount: getColumnCount(width),
      //crossAxisSpacing: 5,
      // Generate 100 widgets that display their index in the List.
      children: List.generate(100, (index) {
        return SizedBox(
          width: 400,
          height: 300,
          child: Card(
            margin: EdgeInsets.all(15.0),
            clipBehavior: Clip.antiAlias,
            elevation: 20,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text("MegaWichtig $index !!!!",
                      style: TextStyle(
                        fontSize: 36 * getFontSize(width),
                        color: Colors.grey[600],
                      )),
                  SizedBox(
                    height: 6.0,
                  ),
                  Text(
                      "Item subtitle with more verbose stuff to fill the card beyond our imagination $index width $width",
                      style: TextStyle(
                        fontSize: 16 * getFontSize(width) , 
                        color: Colors.grey[900])),
                  SizedBox(
                    height: 6,
                  ),
                  Expanded(
                    child: Image.network(
                        "https://linuxwell.files.wordpress.com/2011/08/bridge1.png",
                        fit: BoxFit.scaleDown),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
